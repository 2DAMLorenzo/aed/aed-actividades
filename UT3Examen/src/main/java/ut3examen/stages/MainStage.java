package ut3examen.stages;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import ut3examen.BaseStage;
import ut3examen.DBManager;
import ut3examen.models.Employee;
import ut3examen.models.Office;

import java.io.IOException;
import java.util.Optional;

public class MainStage extends BaseStage {

    public TableView<Office> officesTable;
    public TableColumn<Office, String> officeIDCol;
    public TableColumn<Office, String> officeNameCol;
    public TableColumn<Office, String> officeAddressCol;
    public TableColumn<Office, String> officeLocationCol;
    public TableView<Employee> employeesTable;
    public TableColumn<Employee, String> employeeNifCol;
    public TableColumn<Employee, String> employeeNameCol;
    public TableColumn<Employee, String> employeeAddressCol;
    public TableColumn<Employee, String> employeePositionCol;
    public TableColumn<Employee, String> employeeOfficeCol;
    public Button deleteSelectedBtn;
    public Button editSelectedBtn;
    public Label officesCountLabel;
    public Label employeesCountLabel;

    private ObservableList<Office> offices = FXCollections.observableArrayList();
    private ObservableList<Employee> employees = FXCollections.observableArrayList();

    public void initialize() {
        officeIDCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        officeNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        officeAddressCol.setCellValueFactory(new PropertyValueFactory<>("address"));
        officeLocationCol.setCellValueFactory(new PropertyValueFactory<>("location"));
        officesTable.setItems(offices);
        officesTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Office>() {
            @Override
            public void changed(ObservableValue<? extends Office> observable, Office oldValue, Office newValue) {
                if(newValue != null)
                    employeesTable.getSelectionModel().clearSelection();
            }
        });

        employeeNifCol.setCellValueFactory(new PropertyValueFactory<>("nif"));
        employeeNameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        employeeAddressCol.setCellValueFactory(new PropertyValueFactory<>("address"));
        employeePositionCol.setCellValueFactory(new PropertyValueFactory<>("position"));
        employeeOfficeCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Employee, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Employee, String> param) {
                Office office = param.getValue().getOffice();
                return new SimpleStringProperty(office != null ? office.getName() : "Not set");
            }
        });
        employeesTable.setItems(employees);
        employeesTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Employee>() {
            @Override
            public void changed(ObservableValue<? extends Employee> observable, Employee oldValue, Employee newValue) {
                if(newValue != null)
                    officesTable.getSelectionModel().clearSelection();
            }
        });

        updateTables();
    }

    public void onAddOfficeClick(ActionEvent actionEvent) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/stages/AddOfficeStage.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Add Office");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            updateTables();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onAddEmployeeClick(ActionEvent actionEvent) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/stages/AddEmployeeStage.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Add Employee");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            updateTables();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onDeleteSelectedBtnClick(ActionEvent actionEvent) {
        if(officesTable.getSelectionModel().getSelectedItem() != null) {
            Office wanted = officesTable.getSelectionModel().getSelectedItem();
            Optional res = showAlert(Alert.AlertType.WARNING,
                    "This operation will DELETE all employees assigned to it!\nDo you want to proceed?",
                    "Deleting office with ID " + wanted.getId(), ButtonType.OK, ButtonType.CANCEL);
            if(res.isPresent() && res.get() == ButtonType.OK) {
                DBManager.getInstance().delete(wanted);
            }
        } else if(employeesTable.getSelectionModel().getSelectedItem() != null) {
            Employee wanted = employeesTable.getSelectionModel().getSelectedItem();
            Optional res = showAlert(Alert.AlertType.WARNING,
                    "Do you want to proceed?",
                    "Deleting employee with NIF " + wanted.getNif(), ButtonType.OK, ButtonType.CANCEL);
            if(res.isPresent() && res.get() == ButtonType.OK) {
                DBManager.getInstance().delete(wanted);
            }
        }

        updateTables();
    }

    public void onEditSelectedBtnClick(ActionEvent actionEvent) {
        try {
            if(officesTable.getSelectionModel().getSelectedItem() != null) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/stages/AddOfficeStage.fxml"));
                Parent root = loader.load();
                AddOfficeStage controller = loader.getController();
                controller.setEditMode(officesTable.getSelectionModel().getSelectedItem());
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setTitle("Edit Office");
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.showAndWait();
            } else if(employeesTable.getSelectionModel().getSelectedItem() != null) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/stages/AddEmployeeStage.fxml"));
                Parent root = loader.load();
                AddEmployeeStage controller = loader.getController();
                controller.setEditMode(employeesTable.getSelectionModel().getSelectedItem());
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setTitle("Edit Employee");
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.showAndWait();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        updateTables();
    }

    private void updateTables() {
        offices.clear();
        offices.addAll(DBManager.getInstance().selectAll(Office.class));
        officesCountLabel.setText(offices.size() + " results");

        employees.clear();
        employees.addAll(DBManager.getInstance().selectAll(Employee.class));
        employeesCountLabel.setText(employees.size() + " results");
    }
}

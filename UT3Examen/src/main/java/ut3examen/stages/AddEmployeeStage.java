package ut3examen.stages;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Callback;
import ut3examen.BaseStage;
import ut3examen.DBManager;
import ut3examen.models.Employee;
import ut3examen.models.Office;

public class AddEmployeeStage extends BaseStage {
    public TextField nifField;
    public TextField nameField;
    public TextField addressField;
    public TextField positionField;
    public Button saveButton;
    public Button cancelButton;
    public ComboBox<Office> officeCB;

    private boolean editMode;
    private Employee current;

    public void initialize() {
        ObservableList<Office> departmentNames = FXCollections.observableArrayList();
        departmentNames.addAll(DBManager.getInstance().selectAll(Office.class));

        Callback<ListView<Office>, ListCell<Office>> cellFac = new Callback<ListView<Office>, ListCell<Office>>() {
            @Override
            public ListCell<Office> call(ListView<Office> l) {
                return new ListCell<Office>() {
                    @Override
                    protected void updateItem(Office item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            setText(item.getName());
                        }
                    }
                } ;
            }
        };

        officeCB.setCellFactory(cellFac);
        officeCB.setButtonCell(cellFac.call(null));
        officeCB.setItems(departmentNames);
        officeCB.getSelectionModel().selectFirst();
    }

    public void setEditMode(Employee employee) {
        editMode = true;
        current = employee;

        nifField.setText(employee.getNif());
        nifField.setDisable(true);
        nameField.setText(employee.getName());
        addressField.setText(employee.getAddress());
        positionField.setText(employee.getPosition());
        officeCB.getSelectionModel().select(employee.getOffice());
    }

    public void onSaveClick(ActionEvent actionEvent) {
        if(nifField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "NIF must NOT be NULL!", "Fill the NIF field.");
            return;
        }

        if(!editMode) {
            Employee employee = new Employee();
            employee.setNif(nifField.getText());
            employee.setName(nameField.getText());
            employee.setAddress(addressField.getText());
            employee.setPosition(positionField.getText());
            employee.setOffice(officeCB.getSelectionModel().getSelectedItem());

            DBManager.getInstance().insert(employee);
        } else {
            current.setName(nameField.getText());
            current.setAddress(addressField.getText());
            current.setPosition(positionField.getText());
            current.setOffice(officeCB.getSelectionModel().getSelectedItem());

            DBManager.getInstance().update(current);
        }


        ((Stage)saveButton.getScene().getWindow()).close();
    }

    public void onCancelClick(ActionEvent actionEvent) {
        ((Stage)cancelButton.getScene().getWindow()).close();
    }
}

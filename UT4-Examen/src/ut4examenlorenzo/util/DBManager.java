package ut4examenlorenzo.util;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Query;
import ut4examenlorenzo.Mascota;
import ut4examenlorenzo.Veterinario;

import java.lang.reflect.Array;

/**
 * Created by TheL0w3R on 30/01/2019.
 * All Rights Reserved.
 */
public class DBManager {

    private static DBManager instance;
    private ObjectContainer db;

    public DBManager() {
        instance = this;
        db = Db4oEmbedded.openFile(
                Db4oEmbedded.newConfiguration(), "db_veterinarios_mascotas.yap");
    }

    public static <T> T[] queryAll(Class<T> type) {
        Query query = instance.db.query();
        query.constrain(type);
        ObjectSet<T> data = query.execute();
        return data.toArray((T[]) Array.newInstance(type, 0));
    }

    public static <T> T queryByPK(Class<T> type, String pk) throws InvalidObjectException {
        if(type == Veterinario.class) {
            for(T vet : queryAll(type)) {
                if(((Veterinario)vet).getNombre().trim().equalsIgnoreCase(pk.trim()))
                    return vet;
            }
        } else if(type == Mascota.class) {
            for(T masc : queryAll(type)) {
                if(((Mascota)masc).getChip().trim().equalsIgnoreCase(pk.trim()))
                    return masc;
            }
        } else
            throw new InvalidObjectException();

        return null;
    }

    public static boolean store(Object object) throws InvalidObjectException {
        if(object instanceof Veterinario) {
            Veterinario toInsert = (Veterinario) object;
            for(Veterinario veterinario : queryAll(toInsert.getClass())) {
                if(toInsert.getNombre().trim().equalsIgnoreCase(veterinario.getNombre().trim()))
                    return false;
            }
        } else if(object instanceof Mascota) {
            Mascota toInsert = (Mascota) object;

            for(Mascota mascota : queryAll(toInsert.getClass())) {
                if(toInsert.getChip().equalsIgnoreCase(mascota.getChip()))
                    return false;
            }

            for(Veterinario veterinario : queryAll(Veterinario.class)) {
                if(veterinario.getNombre().trim().equalsIgnoreCase(toInsert.getVeterinario().getNombre().trim())) {
                    toInsert.setVeterinario(veterinario);
                    break;
                }
            }
        } else
            throw new InvalidObjectException();

        instance.db.store(object);
        return true;
    }

    public static void store(Object... objects) throws InvalidObjectException {
        for(Object o : objects)
            store(o);
    }

    public static boolean update(Object object) throws InvalidObjectException {
        if(object instanceof Veterinario) {
            Veterinario veterinario = (Veterinario) object;
            Veterinario[] veterinarios = queryAll(Veterinario.class);
            for(Veterinario current : veterinarios) {
                if(current.getNombre().trim().equalsIgnoreCase(veterinario.getNombre().trim())) {
                    current.setNombre(veterinario.getNombre());
                    current.setDireccion(veterinario.getDireccion());
                    current.setHorario(veterinario.getHorario());
                    instance.db.store(current);
                    return true;
                }
            }
        } else if(object instanceof  Mascota) {
            Mascota mascota = (Mascota) object;
            Mascota[] mascotas = queryAll(Mascota.class);
            for(Mascota current : mascotas) {
                if (current.getChip().trim().equalsIgnoreCase(mascota.getChip().trim())) {
                    current.setChip(mascota.getChip());
                    current.setNombre(mascota.getNombre());
                    current.setRaza(mascota.getRaza());
                    current.setTelefono(mascota.getTelefono());
                    for (Veterinario veterinario : queryAll(Veterinario.class)) {
                        if (mascota.getVeterinario().getNombre().trim().equalsIgnoreCase(veterinario.getNombre().trim())) {
                            current.setVeterinario(veterinario);
                        }
                    }
                    instance.db.store(current);
                    return true;
                }
            }
        } else
            throw new InvalidObjectException();

        return false;
    }

    public static boolean delete(Object object) throws InvalidObjectException {
        if(object instanceof Veterinario) {
            Veterinario veterinario = (Veterinario) object;
            for(Veterinario current : queryAll(Veterinario.class)) {
                if(current.getNombre().trim().equalsIgnoreCase(veterinario.getNombre().trim())) {
                    for(Mascota mascota : queryAll(Mascota.class)) {
                        if(mascota.getVeterinario().getNombre().trim().equalsIgnoreCase(current.getNombre().trim()))
                            instance.db.delete(mascota);
                    }
                    instance.db.delete(current);
                    return true;
                }
            }
        } else if(object instanceof  Mascota) {
            Mascota mascota = (Mascota) object;
            for(Mascota current : queryAll(Mascota.class)) {
                if(current.getChip().trim().equalsIgnoreCase(mascota.getChip().trim())) {
                    instance.db.delete(current);
                    return true;
                }
            }
        } else
            throw new InvalidObjectException();

        return false;
    }
}

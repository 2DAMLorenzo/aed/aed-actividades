package ut4examenlorenzo;

import ut4examenlorenzo.util.DBManager;
import ut4examenlorenzo.util.InvalidObjectException;

public class Extra {

    public static void main(String[] args) throws InvalidObjectException {
        new DBManager();
        System.out.println("Estado actual:");
        printVeterinarios();
        printMascotas();
        System.out.println("\nEliminando a Florencio:");
        for(Veterinario veterinario : DBManager.queryAll(Veterinario.class)) {
            if(veterinario.getNombre().trim().equalsIgnoreCase("Florencio")) {
                System.out.println(DBManager.delete(veterinario) ? "Veterinario eliminado con éxito!" :
                        "Error al eliminar el veterinario!");
            }
        }
        System.out.println();
        printVeterinarios();
        printMascotas();
    }

    private static void printVeterinarios() {
        System.out.println("Veterinarios:");
        for(Veterinario veterinario : DBManager.queryAll(Veterinario.class))
            System.out.println("  - " + veterinario.toString());
    }

    private static void printMascotas() {
        System.out.println("Mascotas:");
        for(Mascota mascota : DBManager.queryAll(Mascota.class))
            System.out.println("  - " + mascota.toString());
    }

}

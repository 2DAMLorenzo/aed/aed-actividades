package ut4examenlorenzo;

import ut4examenlorenzo.util.DBManager;
import ut4examenlorenzo.util.InvalidObjectException;

public class Inserciones {
    public static void main(String[] args) throws InvalidObjectException {
        new DBManager();

        Veterinario laura = new Veterinario("Laura", "C/Alcorac", "LMMMXMJMSN");
        Veterinario ale = new Veterinario("Ale", "Los Tarahales", "MTXTVNMN");
        Veterinario florencio = new Veterinario("Florencio", "Avda. América", "LTMTXMJTSM");

        DBManager.store(laura, ale, florencio);

        Mascota coty = new Mascota("Coty", "112233555", "Labrador", "666111444", ale);
        Mascota max = new Mascota("Max", "888233355", "Pastor Aleman", "928111444", laura);
        Mascota luna = new Mascota("Luna", "888111666", "Pastor Aleman", "555557778", laura);
        Mascota poki = new Mascota("Poki", "111111111", "Siamés", "666111444", florencio);

        DBManager.store(coty, max, luna, poki);


    }
}

package ut4examenlorenzo;

public class Mascota {

    private String nombre, chip, raza, telefono;
    private Veterinario veterinario;

    public Mascota(String nombre, String chip, String raza, String telefono, Veterinario veterinario) {
        this.nombre = nombre;
        this.chip = chip;
        this.raza = raza;
        this.telefono = telefono;
        this.veterinario = veterinario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getChip() {
        return chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Veterinario getVeterinario() {
        return veterinario;
    }

    public void setVeterinario(Veterinario veterinario) {
        this.veterinario = veterinario;
    }

    @Override
    public String toString() {
        return chip + "\n   > Nombre: " + nombre +
                "\n   > Raza: " + raza +
                "\n   > Telefono: " + telefono +
                "\n   > Veterinario: " + veterinario.getNombre() + "\n";
    }
}

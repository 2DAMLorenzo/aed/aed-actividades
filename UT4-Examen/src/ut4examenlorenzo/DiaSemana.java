package ut4examenlorenzo;

public enum DiaSemana {

    L("Lunes"),
    M("Martes"),
    X("Miércoles"),
    J("Jueves"),
    V("Viernes"),
    S("Sábado"),
    D("Domingo");

    private String translated;

    DiaSemana(String translated) {
        this.translated = translated;
    }

    @Override
    public String toString() {
        return translated;
    }
}

package ut5act3.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ut5act3.DataManager;
import ut5act3.FXUtils;
import ut5act3.MainApp;
import ut5act3.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by TheL0w3R on 06/02/2019.
 * All Rights Reserved.
 */
public class MainStageController {
    public MenuItem exportMenuItem;
    public MenuItem importMenuItem;
    public MenuItem closeMenuItem;
    public Button addUserButton;
    public Button removeUserButton;
    public TableView<User> usersTableView;
    public TableColumn<User, String> nameTableColumn;
    public TableColumn<User, String> surnameTableColumn;
    public TableColumn<User, String> emailTableColumn;

    public void initialize() {
        nameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        surnameTableColumn.setCellValueFactory(new PropertyValueFactory<>("surname"));
        emailTableColumn.setCellValueFactory(new PropertyValueFactory<>("email"));

        usersTableView.setItems(DataManager.getInstance().getUserList());

        nameTableColumn.prefWidthProperty().bind(usersTableView.widthProperty().divide(4));
        surnameTableColumn.prefWidthProperty().bind(usersTableView.widthProperty().divide(4));
        emailTableColumn.prefWidthProperty().bind(usersTableView.widthProperty().divide(2));
    }

    public void onExportMenuItemClick(ActionEvent actionEvent) {
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON File", "*.json"));
        File selected = chooser.showSaveDialog(addUserButton.getScene().getWindow());

        if(selected == null)
            return;

        if(!selected.getName().endsWith(".json")) {
            System.out.println(selected.getName());
            FXUtils.showAlert(Alert.AlertType.ERROR, "The selected file is not vaild", "Invalid file!", ButtonType.OK);
            return;
        }

        try {
            DataManager.getInstance().saveData(selected);
        } catch (IOException e) {
            FXUtils.showAlert(
                    Alert.AlertType.ERROR,
                    "An error has occurred trying to save the file: " + e.getMessage(),
                    "Error writing to file!",
                    ButtonType.OK);
        }
    }

    public void onImportMenuItemClick(ActionEvent actionEvent) {
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON File", "*.json"));
        File selected = chooser.showOpenDialog(addUserButton.getScene().getWindow());

        if(selected == null)
            return;

        if(!selected.exists() || !selected.isFile() || !selected.getName().endsWith(".json")) {
            System.out.println(selected.getName());
            FXUtils.showAlert(Alert.AlertType.ERROR, "The selected file is not vaild", "Invalid file!", ButtonType.OK);
            return;
        }

        try {
            DataManager.getInstance().loadData(selected);
        } catch (FileNotFoundException e) {
            FXUtils.showAlert(
                    Alert.AlertType.ERROR,
                    "An error has occurred trying to read the file: " + e.getMessage(),
                    "Error reading the file!",
                    ButtonType.OK);
        }
    }

    public void onCloseMenuItemClick(ActionEvent actionEvent) {
        ((Stage)addUserButton.getScene().getWindow()).close();
    }

    public void onAddUserButtonClick(ActionEvent actionEvent) {
        try {
            Parent root = FXMLLoader.load(MainApp.class.getResource("/AddUserStage.fxml"));
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Add User");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void onRemoveUserButtonClick(ActionEvent actionEvent) {
        User selected = usersTableView.getSelectionModel().getSelectedItem();

        if(selected == null) {
            FXUtils.showAlert(Alert.AlertType.ERROR, "Please, select the User you wish to remove.", "No Users selected!", ButtonType.OK);
            return;
        }

        DataManager.getInstance().removeUser(selected);
    }
}

package ut5act3.controllers;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import ut5act3.DataManager;
import ut5act3.User;

/**
 * Created by TheL0w3R on 06/02/2019.
 * All Rights Reserved.
 */
public class AddUserStageController {
    public TextField nameField;
    public TextField surnameField;
    public TextField emailField;
    public Button createButton;
    public Button cancelButton;

    public void onCreateButtonClick(ActionEvent actionEvent) {
        DataManager.getInstance().addUser(
                new User(nameField.getText(), surnameField.getText(), emailField.getText()));
        ((Stage)nameField.getScene().getWindow()).close();
    }

    public void onCancelButtonClick(ActionEvent actionEvent) {
        ((Stage)nameField.getScene().getWindow()).close();
    }
}

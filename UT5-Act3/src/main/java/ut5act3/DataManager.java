package ut5act3;

import com.google.gson.Gson;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;

/**
 * Created by TheL0w3R on 06/02/2019.
 * All Rights Reserved.
 */
public class DataManager {

    private static DataManager instance;

    private Gson gson;
    private ObservableList<User> userList;

    public DataManager() {
        instance = this;
        gson = new Gson();
        userList = FXCollections.observableArrayList();
    }

    public static DataManager getInstance() {
        return instance;
    }

    public ObservableList<User> getUserList() {
        return userList;
    }

    public void addUser(User user) {
        userList.add(user);
    }

    public void removeUser(User user) {
        userList.remove(user);
    }

    public void saveData(File file) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(gson.toJson(userList.toArray(new User[0])));
        bw.flush();
        bw.close();
    }

    public void loadData(File file) throws FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        User[] users = gson.fromJson(br, User[].class);
        userList.clear();
        userList.addAll(users);
    }
}

package ut1act8;

import com.thoughtworks.xstream.XStream;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by TheL0w3R on 14/10/2018.
 * All Rights Reserved.
 */
public class FileManager {

    private File currentFile;

    public File getCurrentFile() {
        return currentFile;
    }

    public void setFile(File f) {
        this.currentFile = f;
    }

    public void save(School[] schools) {
        try {
            RandomAccessFile raf = new RandomAccessFile(currentFile, "rw");
            StringBuffer sb = null;
            for(School s : schools) {
                raf.writeInt(s.getCode());
                sb = new StringBuffer(s.getName());
                sb.setLength(20); //20 characters, 320 bits, 40 bytes.
                raf.writeChars(sb.toString());
                sb = new StringBuffer(s.getLocation());
                sb.setLength(10); //10 characters, 160 bits, 20 bytes.
                raf.writeChars(sb.toString());
            }
            raf.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "An error has occurred on the file writing procedure.\r\n" + e.getMessage(), "Error writing file", JOptionPane.ERROR_MESSAGE);
        }
    }

    public School searchSchool(int code) {
        try {
            RandomAccessFile raf = new RandomAccessFile(currentFile, "r");
            int pos = 0;
            try {
                while(true) {
                    // 4 + 40 + 20 = 64
                    raf.seek(pos);
                    int currentCode = raf.readInt();
                    if(code == currentCode) {
                        char[] name = new char[20];
                        for(int i = 0; i < name.length; i++) {
                            name[i] = raf.readChar();
                        }
                        char[] location = new char[10];
                        for(int i = 0; i < location.length; i++) {
                            location[i] = raf.readChar();
                        }

                        return new School(currentCode, new String(name), new String(location));
                    }
                    pos += 64;
                }
            } catch (EOFException ignored) {}
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "An error has occurred on the file reading procedure.\r\n" + e.getMessage(), "Error reading file", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    public void modifyName(int id, String name) {
        try {
            RandomAccessFile raf = new RandomAccessFile(currentFile, "rw");
            int pos = 0;
            try {
                while(true) {
                    // 4 + 40 + 20 = total school record
                    raf.seek(pos);
                    int currentID = raf.readInt();
                    if(id == currentID) {
                        pos += 4; // Skipping the ID field.
                        raf.seek(pos);
                        StringBuffer sb = new StringBuffer(name);
                        sb.setLength(20);
                        raf.writeChars(sb.toString());
                    }
                    pos += 64;
                }
            } catch (EOFException ignored) {}
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "An error has occurred on the file reading procedure.\r\n" + e.getMessage(), "Error reading file", JOptionPane.ERROR_MESSAGE);
        }
    }

    private School[] getAllData() {
        ArrayList<School> schools = new ArrayList<>();
        try {
            RandomAccessFile raf = new RandomAccessFile(currentFile, "r");
            int pos = 0;
            while(true) {
                raf.seek(pos);
                int schoolCode = raf.readInt();

                char[] schoolName = new char[20];
                char[] schoolLocation = new char[10];

                for(int i = 0; i < schoolName.length; i++) {
                    schoolName[i] = raf.readChar();
                }

                for(int i = 0; i < schoolLocation.length; i++) {
                    schoolLocation[i] = raf.readChar();
                }

                schools.add(new School(schoolCode, new String(schoolName).trim(), new String(schoolLocation).trim()));
                pos += 64;

                if(raf.getFilePointer() >= raf.length()) break;
            }

            raf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return schools.toArray(new School[]{});
    }

    public void exportXML(File out) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            DOMImplementation imp = db.getDOMImplementation();
            Document doc = imp.createDocument(null, "schools", null);
            doc.setXmlVersion("1.0");

            for(School s : getAllData()) {
                Element root = doc.createElement("school");
                doc.getDocumentElement().appendChild(root);
                createXMLElement("code", String.valueOf(s.getCode()), root, doc);
                createXMLElement("name", s.getName(), root, doc);
                createXMLElement("location", s.getLocation(), root, doc);
            }

            Source fs = new DOMSource(doc);
            Result res = new StreamResult(out);
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(fs, res);
        } catch (ParserConfigurationException | TransformerException e1) {
            JOptionPane.showMessageDialog(null, "An error has occurred during XML exportation procedure.\r\n" + e1.getMessage(), "Error exporting data", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void exportXstream(File out) {
        XStream xstream = new XStream();
        xstream.alias("Schools", Schools.class);
        xstream.alias("School", School.class);
        Schools schools = new Schools();
        schools.schools.addAll(Arrays.asList(getAllData()));
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(out));
            bw.write(xstream.toXML(schools));
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createXMLElement(String data, String value, Element root, Document doc) {
        Element el = doc.createElement(data);
        Text text = doc.createTextNode(value);
        root.appendChild(el);
        el.appendChild(text);
    }

}

package ut1examen;

import ut1examen.util.Window;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

/**
 * Created by thel0w3r on 15/10/2018.
 * All Rights Reserved.
 */
public class MainWindow extends Window {

    private VideogameManager videogameManager;
    private FileManager fileManager;
    private JFileChooser fileChooser;

    private MenuItem newItem;
    private MenuItem openItem;
    private MenuItem saveItem;
    private MenuItem saveAsItem;

    private JButton addBtn, removeBtn, exportBtn, readBtn, modifyBtn;
    private DefaultTableModel defaultModel;
    private JTable table;

    public MainWindow() {
        super("Examen UT1 Lorenzo");
        videogameManager = new VideogameManager();
        fileManager = new FileManager();
        fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Videogame data", "vgames"));
        fileChooser.setAcceptAllFileFilterUsed(false);
        setupMenu();
        setupLayout();
        setupEventHandlers();
        this.setVisible(true);
    }

    private void setupMenu() {
        MenuBar mainMenu = new MenuBar();
        Menu fileMenu = new Menu("File");
        newItem = new MenuItem("New");
        newItem.setShortcut(new MenuShortcut(KeyEvent.VK_N));
        openItem = new MenuItem("Open");
        openItem.setShortcut(new MenuShortcut(KeyEvent.VK_O));
        saveItem = new MenuItem("Save");
        saveItem.setShortcut(new MenuShortcut(KeyEvent.VK_S));
        saveAsItem = new MenuItem("Save As...");
        saveAsItem.setShortcut(new MenuShortcut(KeyEvent.VK_S, true));

        fileMenu.add(newItem);
        fileMenu.addSeparator();
        fileMenu.add(openItem);
        fileMenu.add(saveItem);
        fileMenu.add(saveAsItem);
        mainMenu.add(fileMenu);
        this.setMenuBar(mainMenu);
    }

    private void setupLayout() {
        JLabel title = new JLabel("Videogames");
        positionElement(0, 0, 1, 1, 1, 0, title);
        removeBtn = new JButton("Remove");
        positionElement(1, 0, 1, 1, 0, 0, removeBtn);
        modifyBtn = new JButton("Apply discount");
        positionElement(2, 0, 1, 1, 0, 0, modifyBtn);
        addBtn = new JButton("Add");
        positionElement(3, 0, 1, 1, 0, 0, addBtn);
        defaultModel = new DefaultTableModel(new String[][]{}, new String[]{"Name", "Platform", "Price"}) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        table = new JTable(defaultModel);
        positionElement(0, 1, 4, 1, 1, 1, new JScrollPane(table));
        exportBtn = new JButton("Export DAT file");
        positionElement(0, 2, 2, 1, 1, 0, exportBtn);
        readBtn = new JButton("Read DAT file");
        positionElement(2, 2, 2, 1, 1, 0, readBtn);
    }

    private void setupEventHandlers() {
        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField name = new JTextField();
                JTextField platform = new JTextField();
                JTextField price = new JTextField();
                JPanel panel = new JPanel(new GridLayout(0, 1));
                panel.add(new JLabel("Name:"));
                panel.add(name);
                panel.add(new JLabel("Platform:"));
                panel.add(platform);
                panel.add(new JLabel("Price:"));
                panel.add(price);
                int result = JOptionPane.showConfirmDialog(null, panel, "Add Videogame",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                if (result == JOptionPane.OK_OPTION) {
                    try {
                        videogameManager.addVideogame(new Videogame(name.getText(), platform.getText(), Double.valueOf(price.getText())));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(null, "Price must be a valid number!", "Invalid input", JOptionPane.ERROR_MESSAGE);
                    }
                    updateTable();
                }
            }
        });
        removeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(table.getSelectedRow() != -1) {
                    videogameManager.removeVideogame(table.getSelectedRow());
                    updateTable();
                }
            }
        });
        modifyBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(table.getSelectedRow() != -1) {
                    fileManager.applyDiscount((String)defaultModel.getValueAt(table.getSelectedRow(), 0));
                    videogameManager.clear();
                    videogameManager.loadVideogames(fileManager.read());
                    updateTable();
                }
            }
        });
        exportBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileChooser.setFileFilter(new FileNameExtensionFilter("Videogame object data", "dat"));
                int returnValue = fileChooser.showSaveDialog(MainWindow.this);
                if(returnValue == JFileChooser.APPROVE_OPTION) {
                    String filePath = fileChooser.getSelectedFile().getAbsolutePath();
                    fileManager.exportDAT(videogameManager.getVideogames(), new File(filePath + ((!filePath.endsWith(".dat")) ? ".dat" : "")));
                }
                fileChooser.setFileFilter(new FileNameExtensionFilter("Videogame data", "vgames"));
            }
        });
        readBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileChooser.setFileFilter(new FileNameExtensionFilter("Videogame object data", "dat"));
                int returnValue = fileChooser.showOpenDialog(MainWindow.this);
                if(returnValue == JFileChooser.APPROVE_OPTION) {
                    DefaultTableModel model = new DefaultTableModel(new String[][]{}, new String[]{"Name", "Platform", "Price"}) {
                        @Override
                        public boolean isCellEditable(int row, int column) {
                            return false;
                        }
                    };
                    for(Videogame v : fileManager.readDAT(fileChooser.getSelectedFile())) {
                        model.addRow(v.getRaw());
                    }
                    JTable showTable = new JTable(model);
                    JPanel panel = new JPanel(new GridLayout(0, 1));
                    panel.add(new JScrollPane(showTable));
                    JOptionPane.showMessageDialog(MainWindow.this, panel, "DAT file content", JOptionPane.PLAIN_MESSAGE);
                }
            }
        });
        newItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileManager.unsetFile();
                videogameManager.clear();
                updateTable();
            }
        });
        openItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chooseFile();
                videogameManager.clear();
                videogameManager.loadVideogames(fileManager.read());
                updateTable();
            }
        });
        saveItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileManager.getCurrentFile() == null) {
                    setSaveFile();
                }
                fileManager.save(videogameManager.getVideogames());
            }
        });
        saveAsItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setSaveFile();
                fileManager.save(videogameManager.getVideogames());
            }
        });
    }

    private void chooseFile() {
        int returnValue = fileChooser.showOpenDialog(MainWindow.this);
        if(returnValue == JFileChooser.APPROVE_OPTION) {
            fileManager.setFile(fileChooser.getSelectedFile());
        }
    }

    private void setSaveFile() {
        int returnValue = fileChooser.showSaveDialog(MainWindow.this);
        if(returnValue == JFileChooser.APPROVE_OPTION) {
            String filePath = fileChooser.getSelectedFile().getAbsolutePath();
            fileManager.setFile(new File(filePath + ((!filePath.endsWith(".vgames")) ? ".vgames" : "")));
        }
    }

    private void updateTable() {
        Videogame[] persons = videogameManager.getVideogames();
        defaultModel.setRowCount(0);
        for(Videogame v : persons)
            defaultModel.addRow(v.getRaw());

        defaultModel.fireTableDataChanged();
    }
}

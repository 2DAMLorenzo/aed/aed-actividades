package ut1examen;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by thel0w3r on 15/10/2018.
 * All Rights Reserved.
 */
public class VideogameManager {

    private ArrayList<Videogame> videogames;

    public VideogameManager() {
        videogames = new ArrayList<>();
    }

    public Videogame[] getVideogames() {
        return videogames.toArray(new Videogame[]{});
    }

    public void loadVideogames(Videogame[] vgames) {
        videogames.addAll(Arrays.asList(vgames));
    }

    public void addVideogame(Videogame v) {
        videogames.add(v);
    }

    public void removeVideogame(int index) {
        videogames.remove(index);
    }

    public void clear() {
        videogames.clear();
    }

}

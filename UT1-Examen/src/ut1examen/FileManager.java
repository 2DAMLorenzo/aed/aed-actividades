package ut1examen;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;

/**
 * Created by TheL0w3R on 14/10/2018.
 * All Rights Reserved.
 */
public class FileManager {

    private File currentFile;

    public void unsetFile() {
        currentFile = null;
    }

    public File getCurrentFile() {
        return currentFile;
    }

    public void setFile(File f) {
        this.currentFile = f;
    }

    public void save(Videogame[] videogames) {
        try {
            RandomAccessFile raf = new RandomAccessFile(currentFile, "rw");
            StringBuffer sb;
            for(Videogame v : videogames) {
                sb = new StringBuffer(v.getName());
                sb.setLength(20); // 320 bits, 40 bytes
                raf.writeChars(sb.toString());
                sb = new StringBuffer(v.getPlatform());
                sb.setLength(4); // 64 bits, 8 bytes
                raf.writeChars(sb.toString());
                raf.writeDouble(v.getPrice());
            }
            raf.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "An error has occurred on the file writing procedure.\r\n" + e.getMessage(), "Error writing file", JOptionPane.ERROR_MESSAGE);
        }
    }

    public Videogame[] read() {
        ArrayList<Videogame> videogames = new ArrayList<>();
        try {
            RandomAccessFile raf = new RandomAccessFile(currentFile, "r");
            while(true) {
                char[] name = new char[20];
                char[] platform = new char[4];

                for(int i = 0; i < name.length; i++) {
                    name[i] = raf.readChar();
                }

                for(int i = 0; i < platform.length; i++) {
                    platform[i] = raf.readChar();
                }

                double price = raf.readDouble();

                videogames.add(new Videogame(new String(name).trim(), new String(platform).trim(), price));

                if(raf.getFilePointer() >= raf.length())
                    break;
            }
            return videogames.toArray(new Videogame[]{});
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "An error has occurred on the file reading procedure.\r\n" + e.getMessage(), "Error reading file", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    public void applyDiscount(String name) {
        try {
            RandomAccessFile raf = new RandomAccessFile(currentFile, "rw");
            int pos = 0;
            while(true) {
                raf.seek(pos);
                char[] currentName = new char[20];

                for(int i = 0; i < currentName.length; i++) {
                    currentName[i] = raf.readChar();
                }

                if(new String(currentName).trim().equals(name)) {
                    pos += 48;
                    raf.seek(pos);
                    double price = raf.readDouble();
                    raf.seek(pos);
                    raf.writeDouble(getDiscounted(price));
                    raf.seek(pos);
                    JOptionPane.showMessageDialog(null, "Se ha modificado correctamente el valor " + raf.readDouble(), "Operación completada", JOptionPane.INFORMATION_MESSAGE);
                    break;
                }

                pos += 56;

                if(pos >= raf.length())
                    break;
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "An error has occurred on the file writing procedure.\r\n" + e.getMessage(), "Error writing file", JOptionPane.ERROR_MESSAGE);
        }
    }

    private double getDiscounted(double value) {
        return value - (value * 0.15D);
    }

    public void exportDAT(Videogame[] videogames, File out) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(out));
            for(Videogame v : videogames) {
                oos.writeObject(v);
            }
            oos.flush();
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Videogame[] readDAT(File file) {
        ArrayList<Videogame> videogames = new ArrayList<>();
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            try {
                while(true) {
                    videogames.add((Videogame) ois.readObject());
                }
            } catch (EOFException ignored) {}
            ois.close();
            return videogames.toArray(new Videogame[]{});
        } catch (IOException | ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "An error has occurred on the file reading procedure.\r\n" + e.getMessage(), "Error reading file", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

}

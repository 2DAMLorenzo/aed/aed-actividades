package ut1examen;

import java.io.Serializable;

/**
 * Created by thel0w3r on 15/10/2018.
 * All Rights Reserved.
 */
public class Videogame implements Serializable {

    private String name;
    private String platform;
    private double price;

    public Videogame(String name, String platform, double price) {
        this.name = name;
        this.platform = platform;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String[] getRaw() {
        return new String[] {name, platform, String.valueOf(price)};
    }
}

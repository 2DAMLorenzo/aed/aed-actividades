package ut2examen.menuoptions;

import com.thel0w3r.tlmenuframework.MenuOption;
import ut2examen.Employee;
import ut2examen.Office;
import ut2examen.db.DBManager;

/**
 * Created by thel0w3r on 2018-12-03.
 * All Rights Reserved.
 */
public class ShowData extends MenuOption {

    public ShowData() {
        super("Mostrar datos");
    }

    @Override
    public void run() {
        Office[] offices = DBManager.getInstance().getOffices();
        System.out.println("Oficinas registradas:\n");
        for(Office office : offices)
            System.out.println(office.getCode() + " - " +
                    office.getName() + " - " + office.getAddress() + " - " +
                    office.getLocation());

        System.out.println("\n\nEmpleados:\n");
        for(Employee e : DBManager.getInstance().getEmployees())
            System.out.println(e.getNif() + " - " + e.getName() + " - " +
                    e.getAddress() + " - " + e.getPosition() +
                    "\n   Asignado a la oficina: [" + e.getOffice().getCode() + " - " +
                    e.getOffice().getName() + " - " + e.getOffice().getAddress() + " - " + e.getOffice().getLocation() + "]");
    }
}

package ut1act7;

import ut1act7.util.Window;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by TheL0w3R on 13/10/2018.
 * All Rights Reserved.
 */
public class MainWindow extends Window {

    private FileManager fileManager;
    private JFileChooser fileChooser;

    private JButton openBtn, generateBtn, modifyBtn, searchBtn;
    private DefaultTableModel defaultModel;
    private JTable employeeTable;

    public MainWindow() {
        super("*NO DATA* | Employees RAF");
        fileManager = new FileManager();
        fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Employee data", "emps"));
        fileChooser.setAcceptAllFileFilterUsed(false);
        setupLayout();
        setupEventHandlers();
        this.setVisible(true);
    }

    private void setupLayout() {
        JLabel title = new JLabel("Employees");
        positionElement(0, 0, 2, 1, 1, 0, title);
        generateBtn = new JButton("Generate");
        positionElement(2, 0, 1, 1, 0, 0, generateBtn);
        openBtn = new JButton("Open");
        positionElement(3, 0, 1, 1, 0, 0, openBtn);
        defaultModel = new DefaultTableModel(new String[][]{}, new String[]{"ID", "Name", "Phone", "Salary"}) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        employeeTable = new JTable(defaultModel);
        positionElement(0, 1, 4, 1, 1, 1, new JScrollPane(employeeTable));
        modifyBtn = new JButton("Modify salary");
        positionElement(0, 2, 1, 1, 0, 0, modifyBtn);
        searchBtn = new JButton("Search Employee");
        positionElement(3, 2, 1, 1, 0, 0, searchBtn);
    }

    private void setupEventHandlers() {
        generateBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileManager.getCurrentFile() == null)
                    setSaveFile();

                String[] phones = new String[]{"928647365", "928645387", "667453692", "722546783", "713547384", "900657489", "744356212", "654983784"};
                ArrayList<Employee> employees = new ArrayList<>();
                for(int i = 0; i < 10; i++) {
                    employees.add(new Employee(i + 1, "Employee " + (i+1), phones[new Random().nextInt(phones.length)], 1500));
                }
                fileManager.save(employees.toArray(new Employee[]{}));
            }
        });
        openBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chooseFile();
                defaultModel.setRowCount(0);
            }
        });
        searchBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileManager.getCurrentFile() != null) {
                    String value = JOptionPane.showInputDialog(MainWindow.this, "Enter the employee's ID to search him:");
                    try {
                        Employee em = fileManager.searchEmployee(Integer.valueOf(value));
                        if(em != null)
                            updateTable(em);
                        else
                            JOptionPane.showMessageDialog(MainWindow.this, "The employee ID you specified doesn't exist!", "Unknown Employee", JOptionPane.ERROR_MESSAGE);
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(MainWindow.this, "The employee ID must be a valid Integer!", "Invalid input", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(MainWindow.this, "There is no opened file." +
                            "\r\nTo search employees you'll need to select a file containing employee's data." +
                            "\r\nYou can generate such file by using the \"Generate\" button.", "No data to search", JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        modifyBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileManager.getCurrentFile() != null) {
                    if(employeeTable.getRowCount() < 1)
                        JOptionPane.showMessageDialog(MainWindow.this, "There is no employee selected." +
                                "\r\nTo to select an employee, click on the search button and enter a valid ID.", "No employee selected", JOptionPane.WARNING_MESSAGE);
                    else {
                        String value = JOptionPane.showInputDialog(MainWindow.this, "Enter the new salary:");
                        try {
                            double newSalary = Double.parseDouble(value);
                            int employeeID = Integer.valueOf((String)defaultModel.getValueAt(0, 0));
                            fileManager.modifySalary(employeeID, newSalary);
                            updateTable(fileManager.searchEmployee(employeeID));
                        } catch (NumberFormatException ex) {
                            JOptionPane.showMessageDialog(MainWindow.this, "The value must be a valid Double!", "Invalid input", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(MainWindow.this, "There is no opened file." +
                            "\r\nTo search employees you'll need to select a file containing employee's data." +
                            "\r\nYou can generate such file by using the \"Generate\" button.", "No data to search", JOptionPane.WARNING_MESSAGE);
                }
            }
        });
    }

    private void setSaveFile() {
        int returnValue = fileChooser.showSaveDialog(MainWindow.this);
        if(returnValue == JFileChooser.APPROVE_OPTION) {
            String filePath = fileChooser.getSelectedFile().getAbsolutePath();
            fileManager.setFile(new File(filePath + ((!filePath.endsWith(".emps")) ? ".emps" : "")));
        }
    }

    private void chooseFile() {
        int returnValue = fileChooser.showOpenDialog(MainWindow.this);
        if(returnValue == JFileChooser.APPROVE_OPTION) {
            fileManager.setFile(fileChooser.getSelectedFile());
            this.setTitle(fileChooser.getSelectedFile().getName() + " | Employees RAF");
        }
    }

    private void updateTable(Employee e) {
        defaultModel.setRowCount(0);
        defaultModel.addRow(e.getRaw());
        defaultModel.fireTableDataChanged();
    }

}

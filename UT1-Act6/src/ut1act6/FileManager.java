package ut1act6;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;

/**
 * Created by TheL0w3R on 03/10/2018.
 * All Rights Reserved.
 */
public class FileManager {

    private File currentFile;

    public void unsetFile() {
        currentFile = null;
    }

    public File getCurrentFile() {
        return currentFile;
    }

    public void setFile(File f) {
        this.currentFile = f;
    }

    public void savePersons(Person[] persons) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(currentFile));
            for(Person p : persons) {
                oos.writeObject(p);
            }
            oos.flush();
            oos.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "An error has occurred on the file writing procedure.\r\n" + e.getMessage(), "Error reading file", JOptionPane.ERROR_MESSAGE);
        }
    }

    public Person[] getPersons() {
        ArrayList<Person> persons = new ArrayList<>();
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(currentFile));
            try {
                while(true) {
                    persons.add((Person)ois.readObject());
                }
            } catch (EOFException e) {}
            ois.close();
            return persons.toArray(new Person[]{});
        } catch (IOException | ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "An error has occurred on the file reading procedure.\r\n" + e.getMessage(), "Error reading file", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }
}

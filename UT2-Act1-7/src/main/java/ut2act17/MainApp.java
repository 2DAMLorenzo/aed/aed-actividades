package ut2act17;/**
 * Created by thel0w3r on 2018-11-26.
 * All Rights Reserved.
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ut2act17.database.DatabaseConfig;
import ut2act17.database.DatabaseEngine;
import ut2act17.database.DatabaseManager;

import java.io.IOException;

public class MainApp extends Application {

    public static void main(String[] args) {
        new DatabaseManager();
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/stages/MainStage.fxml"));
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("MultiDB Music Library | " + DatabaseEngine.valueOf(DatabaseConfig.getInstance().getProperty("DBENGINE")));
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

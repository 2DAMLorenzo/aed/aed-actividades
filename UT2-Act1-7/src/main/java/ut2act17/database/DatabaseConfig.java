package ut2act17.database;

import java.io.*;
import java.util.Properties;
import java.util.prefs.Preferences;

/**
 * Created by TheL0w3R on 27/11/2018.
 * All Rights Reserved.
 */
public class DatabaseConfig {

    private static DatabaseConfig instance;

    private File configFile;
    private Properties config;

    public DatabaseConfig() {
        instance = this;
        configFile = new File("config.properties");
        Properties defaults = new Properties();
        defaults.setProperty("DBENGINE", "SQLITE");
        defaults.setProperty("SQLITE_DBPATH", "./musicstore.db");
        defaults.setProperty("MYSQL_HOST", "localhost");
        defaults.setProperty("MYSQL_PORT", "3306");
        defaults.setProperty("MYSQL_USER", "root");
        defaults.setProperty("MYSQL_PASSWORD", "secret");
        defaults.setProperty("MYSQL_DATABASE", "musicstore");
        defaults.setProperty("ORACLE_HOST", "localhost");
        defaults.setProperty("ORACLE_PORT", "1521");
        defaults.setProperty("ORACLE_USER", "root");
        defaults.setProperty("ORACLE_PASSWORD", "secret");
        defaults.setProperty("ORACLE_WORKSPACE", "workspace");
        defaults.setProperty("DERBY_HOST", "localhost");
        defaults.setProperty("DERBY_PORT", "1527");
        defaults.setProperty("DERBY_DATABASE", "musicstore");
        config = new Properties(defaults);
        load();
    }

    public static DatabaseConfig getInstance() {
        return instance;
    }

    public String getProperty(String propKey) {
        return config.getProperty(propKey);
    }

    public void setProperty(String key, String value) {
        config.setProperty(key, value);
    }

    public void save() {
        try {
            OutputStream os = new FileOutputStream(configFile);
            config.store(os, "MultiDB Music Library config file");
            System.out.println("SAVED!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load() {
        try {
            if(!configFile.exists()) {
                save();
            }
            FileInputStream fis = new FileInputStream(configFile);
            config.load(fis);
            System.out.println("TESTING PROPERTIES: " + config.getProperty("DBENGINE"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

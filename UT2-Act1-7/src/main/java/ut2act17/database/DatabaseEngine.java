package ut2act17.database;

/**
 * Created by TheL0w3R on 27/11/2018.
 * All Rights Reserved.
 */
public enum DatabaseEngine {

    SQLITE("SQLite"),
    MYSQL("MySQL"),
    DERBY("Derby"),
    ORACLE("Oracle");

    private String name;

    private DatabaseEngine(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}

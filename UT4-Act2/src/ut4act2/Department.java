package ut4act2;

/**
 * Created by TheL0w3R on 30/01/2019.
 * All Rights Reserved.
 */
public class Department {

    private String name;
    private String location;

    public Department(String name, String location) {
        this.name = name;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}

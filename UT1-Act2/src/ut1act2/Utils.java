package ut1act2;

import java.io.File;

/**
 * Created by TheL0w3R on 25/09/2018.
 * All Rights Reserved.
 */
public class Utils {

    public static String getPermissions(File f) {
        String permissions = "";
        if(f.canRead())
            permissions += " Read";

        if(f.canWrite())
            permissions += " Write";

        return permissions.trim();
    }

    public static double toMegaBytes(long bytes) {
        return (double)Math.round(((bytes / 1024D) / 1024D) * 1000D) / 1000D;
    }

}

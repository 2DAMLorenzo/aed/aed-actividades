package ut1act3;

import javax.swing.*;
import java.io.*;
import java.nio.charset.Charset;

/**
 * Created by TheL0w3R on 03/10/2018.
 * All Rights Reserved.
 */
public class FileManager {

    private final String CHARSET = "UTF-8";

    private File currentFile;

    public void unsetFile() {
        currentFile = null;
    }

    public File getCurrentFile() {
        return currentFile;
    }

    public void setFile(File f) {
        this.currentFile = f;
    }

    public String readFile() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(currentFile), Charset.forName(CHARSET)));
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = br.readLine()) != null) {
                sb.append(line).append("\r\n");
            }
            br.close();
            return sb.toString();
        } catch (IOException e1) {
            JOptionPane.showMessageDialog(null, "An error has occurred trying to read the file.\r\n" + e1.getMessage(), "Error opening file", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    public void writeFile(String text) {
        try {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(currentFile), Charset.forName(CHARSET)));
            bw.write(text);
            bw.flush();
            bw.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "An error has occurred trying to save the file.\r\n" + e.getMessage(), "Error saving file", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void appendToFile(String text) {
        try {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(currentFile, true), Charset.forName(CHARSET)));
            bw.write("\r\n" + text);
            bw.flush();
            bw.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "An error has occurred trying to save the file.\r\n" + e.getMessage(), "Error saving file", JOptionPane.ERROR_MESSAGE);
        }
    }
}

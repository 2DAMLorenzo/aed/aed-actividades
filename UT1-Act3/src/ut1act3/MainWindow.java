package ut1act3;

import ut1act3.util.Window;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.*;
import java.nio.charset.Charset;

/**
 * Created by thel0w3r on 01/10/2018.
 * All Rights Reserved.
 */
public class MainWindow extends Window {

    private JTextArea fileContent;
    private JFileChooser fileChooser;

    private FileManager fileManager;

    public MainWindow() {
        super("Untitled: AmazingPad");
        fileManager = new FileManager();
        setupWindowMenu();
        setupLayout();
        this.setVisible(true);
    }

    private void setupWindowMenu() {
        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("File");
        MenuItem newFile = new MenuItem("New");
        newFile.setShortcut(new MenuShortcut(KeyEvent.VK_N));
        newFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileContent.setText("");
                MainWindow.this.setTitle("Untitled: AmazingPad");
                fileManager.unsetFile();
            }
        });
        MenuItem openFile = new MenuItem("Open");
        openFile.setShortcut(new MenuShortcut(KeyEvent.VK_O));
        openFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chooseFile();
                fileContent.setText(fileManager.readFile());
                MainWindow.this.setTitle(fileManager.getCurrentFile().getName() + ": AmazingPad");
            }
        });
        MenuItem saveFile = new MenuItem("Save");
        saveFile.setShortcut(new MenuShortcut(KeyEvent.VK_S));
        saveFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileManager.getCurrentFile() == null) {
                    writeFileAs();
                    MainWindow.this.setTitle(fileManager.getCurrentFile().getName() + ": AmazingPad");
                }
                else
                    fileManager.writeFile(fileContent.getText());
            }
        });
        MenuItem saveFileAs = new MenuItem("Save as...");
        saveFileAs.setShortcut(new MenuShortcut(KeyEvent.VK_S, true));
        saveFileAs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                writeFileAs();
                MainWindow.this.setTitle(fileManager.getCurrentFile().getName() + ": AmazingPad");
            }
        });
        Menu extraMenu = new Menu("Extra");
        MenuItem extraSubMenu = new MenuItem("Add to EOF");
        extraSubMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileManager.getCurrentFile() != null) {
                    fileManager.appendToFile("y esto es el final");
                    fileContent.setText(fileManager.readFile());
                } else
                    JOptionPane.showMessageDialog(null, "There isn't any opened file", "No file open", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        fileMenu.add(newFile);
        fileMenu.addSeparator();
        fileMenu.add(openFile);
        fileMenu.add(saveFile);
        fileMenu.add(saveFileAs);
        extraMenu.add(extraSubMenu);
        menuBar.add(fileMenu);
        menuBar.add(extraMenu);
        this.setMenuBar(menuBar);
    }

    private void setupLayout() {
        fileContent = new JTextArea();
        positionElement(0, 0, 1, 1, 1, 1, new Insets(0, 0, 0, 0), GridBagConstraints.BOTH, new JScrollPane(fileContent));
        fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Text documents", "txt"));
        fileChooser.setAcceptAllFileFilterUsed(false);
    }

    private void chooseFile() {
        int returnValue = fileChooser.showOpenDialog(MainWindow.this);
        if(returnValue == JFileChooser.APPROVE_OPTION) {
            fileContent.setText("");
            fileManager.setFile(fileChooser.getSelectedFile());
        }
    }

    private void writeFileAs() {
        int returnValue = fileChooser.showSaveDialog(MainWindow.this);
        if(returnValue == JFileChooser.APPROVE_OPTION) {
            String filePath = fileChooser.getSelectedFile().getAbsolutePath();
            fileManager.setFile(new File(filePath + ((!filePath.endsWith(".txt")) ? ".txt" : "")));
        }
        fileManager.writeFile(fileContent.getText());
    }
}
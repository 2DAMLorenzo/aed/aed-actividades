package ut3act1.models;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
public class Departamentos {
    private int id;
    private String location;
    private String name;
    private Collection<Empleados> empleados;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "LOCATION")
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Departamentos that = (Departamentos) o;
        return id == that.id &&
                Objects.equals(location, that.location) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, location, name);
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "departamento")
    public Collection<Empleados> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(Collection<Empleados> empleados) {
        this.empleados = empleados;
    }
}

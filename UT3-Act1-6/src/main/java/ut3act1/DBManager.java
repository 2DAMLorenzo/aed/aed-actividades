package ut3act1;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import ut3act1.models.Departamentos;
import ut3act1.models.Empleados;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class DBManager {

    private static DBManager instance;

    private SessionFactory sessionFactory;

    public DBManager() {
        instance = this;
        sessionFactory = new Configuration().configure()
                .setProperty("hibernate.connection.username", "system")
                .setProperty("hibernate.connection.password", "root")
                .setProperty("hibernate.default_schema", "ACT2UT3").buildSessionFactory();
    }

    public static DBManager getInstance() {
        return instance;
    }

    public void addDepartment(int id, String name, String location) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Departamentos d = new Departamentos();
        d.setId(id);
        d.setName(name);
        d.setLocation(location);
        session.save(d);
        transaction.commit();
    }

    public void updateDepartment(Departamentos dep) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.update(dep);
        transaction.commit();
    }

    public void deleteDepartment(Departamentos dep) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.delete(dep);
        transaction.commit();
        session.close();
    }

    public void addEmployee(int id, String name, String position, double salary, double commission, Date ingrressDate, Departamentos departamento) {
        //Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Empleados e = new Empleados();
        e.setId(id);
        e.setName(name);
        e.setPosition(position);
        e.setSalary(salary);
        e.setComision(commission);
        e.setIngressDate(ingrressDate);
        e.setDepartamento(departamento);
        session.save(e);
        transaction.commit();
    }

    public void updateEmployee(Empleados emp) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.update(emp);
        transaction.commit();
    }

    public void deleteEmployee(Empleados emp) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.delete(emp);
        transaction.commit();
    }

    public void deleteEmployees(Collection<Empleados> employees) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        for(Empleados e : employees)
            session.delete(e);
        transaction.commit();
    }

    public <T> List<T> loadAllData(Class<T> type) {
        Session session = sessionFactory.getCurrentSession();
        Transaction t = session.beginTransaction();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(type);
        criteria.from(type);
        List<T> data = session.createQuery(criteria).getResultList();
        t.commit();
        return data;
    }
}

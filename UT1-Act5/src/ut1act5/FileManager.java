package ut1act5;

import javax.swing.*;
import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;

/**
 * Created by TheL0w3R on 03/10/2018.
 * All Rights Reserved.
 */
public class FileManager {

    private File currentFile;

    public void unsetFile() {
        currentFile = null;
    }

    public File getCurrentFile() {
        return currentFile;
    }

    public void setFile(File f) {
        this.currentFile = f;
    }

    public void saveSmartphones(Smartphone[] smartphones) {
        try {
            DataOutputStream dos = new DataOutputStream(new FileOutputStream(currentFile));
            for(Smartphone s : smartphones) {
                dos.writeUTF(s.getName());
                dos.writeUTF(s.getOs());
                dos.writeDouble(s.getMah());
            }
            dos.flush();
            dos.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "An error has occurred on the file writing procedure.\r\n" + e.getMessage(), "Error reading file", JOptionPane.ERROR_MESSAGE);
        }
    }

    public Smartphone[] readFile() {
        ArrayList<Smartphone> smartphones = new ArrayList<>();
        try {
            DataInputStream dis = new DataInputStream(new FileInputStream(currentFile));
            try {
                while(true) {
                    smartphones.add(new Smartphone(dis.readUTF(), dis.readUTF(), dis.readDouble()));
                }
            } catch (EOFException e) {
                dis.close();
            }
            return smartphones.toArray(new Smartphone[]{});
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "An error has occurred on the file reading procedure.\r\n" + e.getMessage(), "Error reading file", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }
}

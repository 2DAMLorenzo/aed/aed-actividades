package ut1act5;

import ut1act5.util.Window;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

/**
 * Created by thel0w3r on 04/10/2018.
 * All Rights Reserved.
 */
public class MainWindow extends Window {

    private SmartphoneManager smartphoneManager;
    private FileManager fileManager;
    private JFileChooser fileChooser;

    private MenuItem newItem;
    private MenuItem openItem;
    private MenuItem saveItem;
    private MenuItem saveAsItem;

    private JButton addBtn;
    private JButton removeBtn;
    private DefaultTableModel defaultModel;
    private JTable smarTable;

    public MainWindow() {
        super("Smartphones Database");
        smartphoneManager = new SmartphoneManager();
        fileManager = new FileManager();
        fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Smartphones data", "smdat"));
        fileChooser.setAcceptAllFileFilterUsed(false);
        setupMenu();
        setupLayout();
        setupEventHandlers();
        this.setVisible(true);
    }

    private void setupMenu() {
        MenuBar mainMenu = new MenuBar();
        Menu fileMenu = new Menu("File");
        newItem = new MenuItem("New");
        newItem.setShortcut(new MenuShortcut(KeyEvent.VK_N));
        openItem = new MenuItem("Open");
        openItem.setShortcut(new MenuShortcut(KeyEvent.VK_O));
        saveItem = new MenuItem("Save");
        saveItem.setShortcut(new MenuShortcut(KeyEvent.VK_S));
        saveAsItem = new MenuItem("Save As...");
        saveAsItem.setShortcut(new MenuShortcut(KeyEvent.VK_S, true));

        fileMenu.add(newItem);
        fileMenu.addSeparator();
        fileMenu.add(openItem);
        fileMenu.add(saveItem);
        fileMenu.add(saveAsItem);
        mainMenu.add(fileMenu);
        this.setMenuBar(mainMenu);
    }

    private void setupLayout() {
        JLabel smartLabel = new JLabel("Smartphone list");
        positionElement(0, 0, 2, 1, 1, 0, smartLabel);
        removeBtn = new JButton("Remove");
        positionElement(2, 0, 1, 1, 0, 0, removeBtn);
        addBtn = new JButton("Add");
        positionElement(3, 0, 1, 1, 0, 0, addBtn);
        defaultModel = new DefaultTableModel(new String[][]{}, new String[]{"Name", "Operating System", "Battery (mAh)"}) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        smarTable = new JTable(defaultModel);
        positionElement(0, 1, 4, 1, 1, 1, new JScrollPane(smarTable));
    }

    private void setupEventHandlers() {
        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField name = new JTextField();
                JTextField os = new JTextField();
                JTextField mah = new JTextField();
                JPanel panel = new JPanel(new GridLayout(0, 1));
                panel.add(new JLabel("Name:"));
                panel.add(name);
                panel.add(new JLabel("Operating System:"));
                panel.add(os);
                panel.add(new JLabel("Battery (mAh)"));
                panel.add(mah);
                int result = JOptionPane.showConfirmDialog(null, panel, "Add Smartphone",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                if (result == JOptionPane.OK_OPTION) {
                    smartphoneManager.addSmartphone(new Smartphone(name.getText(), os.getText(), Double.parseDouble(mah.getText())));
                    updateTable();
                }
            }
        });
        removeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                smartphoneManager.removeSmartphone(smarTable.getSelectedRow());
                updateTable();
            }
        });
        newItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileManager.unsetFile();
                smartphoneManager.clearSmartphones();
                updateTable();
            }
        });
        openItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chooseFile();
                smartphoneManager.loadSmartphones(fileManager.readFile());
                updateTable();
            }
        });
        saveItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(fileManager.getCurrentFile() == null) {
                    setSaveFile();
                }
                fileManager.saveSmartphones(smartphoneManager.getSmartphones());
            }
        });
        saveAsItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setSaveFile();
                fileManager.saveSmartphones(smartphoneManager.getSmartphones());
            }
        });
    }

    private void updateTable() {
        Smartphone[] smartphones = smartphoneManager.getSmartphones();
        defaultModel.setRowCount(0);
        for(Smartphone s : smartphones)
            defaultModel.addRow(s.getRaw());

        defaultModel.fireTableDataChanged();
    }

    private void chooseFile() {
        int returnValue = fileChooser.showOpenDialog(MainWindow.this);
        if(returnValue == JFileChooser.APPROVE_OPTION) {
            fileManager.setFile(fileChooser.getSelectedFile());
        }
    }

    private void setSaveFile() {
        int returnValue = fileChooser.showSaveDialog(MainWindow.this);
        if(returnValue == JFileChooser.APPROVE_OPTION) {
            String filePath = fileChooser.getSelectedFile().getAbsolutePath();
            fileManager.setFile(new File(filePath + ((!filePath.endsWith(".smdat")) ? ".smdat" : "")));
        }
    }
}

package ut1act5;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by thel0w3r on 08/10/2018.
 * All Rights Reserved.
 */
public class SmartphoneManager {

    private ArrayList<Smartphone> smartphones;

    public SmartphoneManager() {
        smartphones = new ArrayList<>();
    }

    public Smartphone[] getSmartphones() {
        return smartphones.toArray(new Smartphone[]{});
    }

    public void addSmartphone(Smartphone s) {
        smartphones.add(s);
    }

    public void removeSmartphone(int index) {
        smartphones.remove(index);
    }

    public void loadSmartphones(Smartphone[] smarts) {
        smartphones.addAll(Arrays.asList(smarts));
    }

    public void clearSmartphones() {
        this.smartphones.clear();
    }
}

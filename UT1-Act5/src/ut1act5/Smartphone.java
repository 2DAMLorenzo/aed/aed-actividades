package ut1act5;

/**
 * Created by thel0w3r on 08/10/2018.
 * All Rights Reserved.
 */
public class Smartphone {

    private String name;
    private String os;
    private Double mah;

    public Smartphone(String name, String os, Double mah) {
        this.name = name;
        this.os = os;
        this.mah = mah;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public Double getMah() {
        return mah;
    }

    public void setMah(Double mah) {
        this.mah = mah;
    }

    public String[] getRaw() {
        return new String[]{name, os, Double.toString(mah)};
    }
}
